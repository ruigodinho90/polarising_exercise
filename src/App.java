import java.util.Scanner;

public class App {

    Scanner scanner = new Scanner(System.in);
    Randomizer randomizer = new Randomizer();
    Fibonacci fibonacci = new Fibonacci();
    PrimesAndOdds primesAndOdds = new PrimesAndOdds();

    public void start() {
        System.out.println("Please, insert two values for an interval of numbers: ");
        System.out.print("Minimum value: ");
        int min = scanner.nextInt();
        System.out.print("Maximum value: ");
        int max = scanner.nextInt();

        int randomNumber = randomizer.generateRandomNumber(min, max);

        int[] fiboArray = fibonacci.fiboSequence(randomNumber);
        //System.out.print("Fibonacci Sequence: ");
        /*for(int fibonumbers : fiboArray) {
            System.out.print(fibonumbers + " ");
        }*/

        primesAndOdds.checkPrimesAndOdds(fiboArray);
    }
}
