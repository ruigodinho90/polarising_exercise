public class Fibonacci {

    public int[] fiboSequence(int randomNumber) {
        if(randomNumber <= 0) {
            return new int[0];
        }

        int[] sampleArray = new int[randomNumber];
        /*int index = 0;
        int fiboNumber = fibonacci(index);

        while (fiboNumber <= randomNumber) {
            sampleArray[index++] = fiboNumber;
            fiboNumber = fibonacci(index);
        }*/

        System.out.print("Fibonacci Sequence: ");
        for(int i = 0; i < sampleArray.length; i++) {
            sampleArray[i] = fibonacci(i);
            System.out.print(sampleArray[i] + " ");
        }

        return sampleArray;
        /*int[] fibonacciArray = new int[index];
        System.arraycopy(sampleArray,0,fibonacciArray,0, index);
        return fibonacciArray;*/
    }

    private int fibonacci(int number) {
        if(number <= 1) {
            return number;
        }
        return fibonacci(number - 1) + fibonacci(number - 2);
    }
}
