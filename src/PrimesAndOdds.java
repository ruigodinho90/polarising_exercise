public class PrimesAndOdds {

    public void checkPrimesAndOdds(int[] numbers) {
        int primesFound = 0;
        int oddsFound = 0;

        for(int number : numbers) {
            if(getPrimes(number)) {
                primesFound++;
            }
            if(getOdds(number)) {
                oddsFound++;
            }
        }

        System.out.println("\nTotal prime numbers: " + primesFound);
        System.out.println("Total odd numbers: " + oddsFound);
    }

    private boolean getOdds(int number) {
        return number % 2 != 0;
    }

    private boolean getPrimes(int number) {
        if(number <= 1) {
            return false;
        }
        for(int i = 2; i <= number / 2; i++) {
            if(number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
