
public class Randomizer {

    public int generateRandomNumber (int min, int max) {
        int randomNumber = (int) (Math.random() * (max - min + 1) + min);
        System.out.println("Random Generated Number: " + randomNumber);
        return randomNumber;
    }
}
